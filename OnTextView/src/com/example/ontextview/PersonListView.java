package com.example.ontextview;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;

import com.example.ontextview.widget.BaseOnTextView;

public class PersonListView extends BaseOnTextView<Person>{

	public PersonListView(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
	}

	public PersonListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public PersonListView(Context context) {
		super(context);
	}
	
	public void setVoteName(ArrayList<Person> list,int index){
		this.getInfo(list);
		setVoteList(list,index);
	}

	@Override
	public String getVoteName(Person data) {
		return data.name;
	}

	@Override
	public List<Person> getInfo(List<Person> list) {
		return list;
	}



	

}
