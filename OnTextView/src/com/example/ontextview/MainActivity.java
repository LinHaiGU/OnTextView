﻿package com.example.ontextview;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

public class MainActivity extends Activity {

	private ListView lv_lsit;
	private ArrayList<Person> personList=new ArrayList<Person>();
	private PersonListAdapter mPersonListAdapter=new PersonListAdapter();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initView();
	}
	
	private void initView(){
		String[] strs = {"火影忍者", "卡卡西", "漩涡鸣人", "宇智波鼬" ,"宇智波佐助","小樱","李洛克","大蛇丸","取个名字好难啊","请不要再来伤害我"};
		for(int i=0;i<strs.length;i++){
			Person obj=new Person();
			obj.name=strs[i];
			personList.add(obj);
		}
		
		lv_lsit=(ListView)findViewById(R.id.lv_lsit);
		lv_lsit.setAdapter(mPersonListAdapter);
		mPersonListAdapter.notifyDataSetChanged();
	}

	
	class PersonListAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			return 1;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder viewHolder;
			if(convertView==null){
				viewHolder=new ViewHolder();
				convertView=LayoutInflater.from(MainActivity.this).inflate(R.layout.person_item, null);
				viewHolder.tv_vote_names=(PersonListView)convertView.findViewById(R.id.tv_vote_names);
				convertView.setTag(viewHolder);
			}else{
				viewHolder=(ViewHolder) convertView.getTag();
			}
			
			viewHolder.tv_vote_names.setVoteList(personList, 0);
			return convertView;
		}
		
	}
	
	static class ViewHolder{
		PersonListView tv_vote_names;
	}
	
}
