package com.example.ontextview.widget;

import java.util.List;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;


public abstract class BaseOnTextView<T> extends TextView implements
		SelectNameList<T> {
	private Context mContext;
	public BaseOnTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.mContext=context;
		initView();
	}

	public BaseOnTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.mContext=context;
		initView();
	}

	public BaseOnTextView(Context context) {
		super(context);
		this.mContext=context;
		initView();
	}

	private void initView() {
		setMovementMethod(LinkMovementMethod.getInstance());
		setClickable(false);
		setFocusable(false);
		setPressed(false);
		setLongClickable(false);
		setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
	}

	
	/**
	 * 设置点赞列表
	 * @param datas 数据
	 * @param clickIndex 响应的第几个事件
	 */
	public void setVoteList(List<T> datas,int clickIndex) {
		setText("");
		int length=datas.size();
		for (int i = 0; i < length; i++) {
			T data = datas.get(i);
			String text_name=getVoteName(data);
			if (i == length - 1) {
				SpannableStringBuilder stylesBuilder = new SpannableStringBuilder(
						text_name);
				TextViewSpan myURLSpan = new TextViewSpan(text_name,mContext,clickIndex);
				myURLSpan.setInfo(getInfo(datas).get(i));
				stylesBuilder.setSpan(myURLSpan, 0, text_name.length(),
						Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				append(stylesBuilder);
			} else {
				SpannableStringBuilder stylesBuilder = new SpannableStringBuilder(
						text_name + ",");
				TextViewSpan myURLSpan = new TextViewSpan(text_name + ",",mContext,clickIndex);
				myURLSpan.setInfo(getInfo(datas).get(i));
				stylesBuilder.setSpan(myURLSpan, 0, text_name.length() + 1,
						Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				append(stylesBuilder);
			}
		}
	}

}