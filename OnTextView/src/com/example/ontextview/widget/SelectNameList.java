package com.example.ontextview.widget;

import java.util.List;

public interface SelectNameList<T> {
	/**
	 * 获取点赞姓名
	 * @param data
	 * @return
	 */
	public abstract String getVoteName(T data);
	
	
	/**
	 * 获取点赞人的所有信息
	 * @return
	 */
	public abstract List<T> getInfo(List<T> list);
	
}
