package com.example.ontextview.widget;

import android.content.Context;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Toast;

import com.example.ontextview.Person;
import com.example.ontextview.R;

public class TextViewSpan<T> extends ClickableSpan{
		private String clickString;
		private Context mContext;
		private int selectClick;
		private T votePerson;
		
		
		public TextViewSpan(String clickString,Context context,int selectClick) {
			this.clickString = clickString;
			this.mContext=context;
			this.selectClick=selectClick;
		}

		
		/**
		 * 设置点赞人的信息
		 * @param t
		 */
		public void setInfo(T t){
			votePerson=t;
		}
		
		@Override
		public void updateDrawState(TextPaint ds) {
			ds.setColor(mContext.getResources().getColor(
					R.color.main_link));
			ds.setUnderlineText(false); // 去掉下划�?
		}

		@Override
		public void onClick(View widget) {
			switch (selectClick) {
			case 0://打开个人主界面
				Person person=(Person) votePerson;
				Toast.makeText(mContext, person.name,
						Toast.LENGTH_SHORT).show();
				break;
			case 1:
				break;
			default:
				break;
			}
			
		}

}
